package com.example.ronal.segundapracticajavier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Pasar_Parametro extends AppCompatActivity {
 EditText EditText etNombre, etApellido, etTelefono, etEmail;
    Button buttonE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar__parametro);
        etNombre = (EditText) findViewById(R.id.etNombre);
        etApellido = (EditText) findViewById(R.id.etApellido);
        etTelefono = (EditText) findViewById(R.id.etTelefono);
        etEmail = (EditText) findViewById(R.id.etEmail);
        botonEnviar = (Button) findViewById(R.id.buttonEnviarParametro);

        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        actividad_pasar_parametro.this, actividad_recibir_parametro.class
                );
                Bundle bundle = new Bundle();
                bundle.putString("nombre",etNombre.getText().toString());
                bundle.putString("apellido",etApellido.getText().toString());
                bundle.putString("telefono",etTelefono.getText().toString());
                bundle.putString("email",etEmail.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
