package com.example.ronal.segundapracticajavier;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class Recibir_Parametro extends AppCompatActivity {
    EditText etNombres, etApellidos, etTelefono1, etEmails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir__parametro);
        etNombres = findViewById(R.id.etNombres);
        etApellidos = findViewById(R.id.etApellidos);
        etTelefono1 = findViewById(R.id.etTelefono1);
        etEmails = findViewById(R.id.etEmails);
        Bundle bundle = this.getIntent().getExtras();
        etNombres.setText(bundle.getString("nombre"));
        etApellidos.setText(bundle.getString("apellido"));
        etTelefono1.setText(bundle.getString("telefono"));
        etEmails.setText(bundle.getString("email"));
    }
}
